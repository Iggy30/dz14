﻿#include<string>
#include <iostream>

int main()
{
    std::string std = "Hello!";

    std::cout << std << "\n";

    std::cout << std.length() << "\n";

    std::cout << std[0] << "\n";

    std::cout << std[std.length() - 1] << "\n";
}